const port = 8080;
const ip = "";

var http = require("http");

http.createServer((req, res) => {
    console.log("Request.");
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Hello World\n");
}).listen(port, ip);

console.log(`Server running at http://${ip}:${port}`);
